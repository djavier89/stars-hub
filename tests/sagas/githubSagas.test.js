import {put, fork, select} from 'redux-saga/effects'
import {cloneableGenerator} from 'redux-saga/utils'
import * as githubActions from '../../src/actions/github/actions'
import * as githubSagas from '../../src/sagas/githubSagas'

const request = {
  username: 'djavier'
}

describe('login flow', () => {
  test('get the profile successful', () => {
    const generator = cloneableGenerator(githubSagas.githubGetProfile)(request)
    expect(generator.next().value).toEqual(put(githubActions.profileLoading()))
    const clone = generator.clone()
    clone.next(true)
    expect(clone.next(true).value).toEqual(put(githubActions.profileSuccess(true)))
    expect(clone.next(true).value).toEqual(fork(githubSagas.githubAddSearchHistory, true))
    expect(clone.next().done).toEqual(true)
  })

  test('get the starred repos successful', () => {
    const generator = cloneableGenerator(githubSagas.githubGetStarredByUsername)(request)
    expect(generator.next().value).toEqual(put(githubActions.starredLoading()))
    const clone = generator.clone()
    clone.next(true)
    expect(clone.next(true).value).toEqual(put(githubActions.starredSuccess(true)))
    expect(clone.next().done).toEqual(true)
  })

  test('get the following repos successful', () => {
    const generator = cloneableGenerator(githubSagas.githubGetFollowingByUsername)(request)
    expect(generator.next().value).toEqual(put(githubActions.followingLoading()))
    const clone = generator.clone()
    clone.next(true)
    expect(clone.next(true).value).toEqual(put(githubActions.followingSuccess(true)))
    expect(clone.next().done).toEqual(true)
  })

  test('get the followers repos successful', () => {
    const generator = cloneableGenerator(githubSagas.githubGetFollowersByUsername)(request)
    expect(generator.next().value).toEqual(put(githubActions.followersLoading()))
    const clone = generator.clone()
    clone.next(true)
    expect(clone.next(true).value).toEqual(put(githubActions.followersSuccess(true)))
    expect(clone.next().done).toEqual(true)
  })

  test('get the extended profile gather all the information', () => {
    const generator = cloneableGenerator(githubSagas.githubExtendedProfile)(request)
    expect(generator.next().value).toEqual(select(githubSagas.profileSelector))
    expect(generator.next().value).toEqual(fork(githubSagas.githubGetProfile, request))
    expect(generator.next().value).toEqual(fork(githubSagas.githubGetStarredByUsername, request))
    expect(generator.next().value).toEqual(fork(githubSagas.githubGetFollowingByUsername, request))
    expect(generator.next().value).toEqual(fork(githubSagas.githubGetFollowersByUsername, request))
    expect(generator.next().done).toEqual(true)
  })
})
