# StarsHub

Simple site to look for your github profile, starred projects, following and followers users.

Built upon the following dependencies:

- `React@^16`
- `webpack@^4` to bundle the necessary assets
- `webpack-dev-server` to spin up our local development server
- `babel@^6` with `babel-preset-env`, `babel-preset-react` and `babel-loader` to compile our ES6 code base to plain old compatible javascript.
- `react-redux` to manage application state
- `redux-saga` to handle side effects,
- `axios` to send request to Github Open API
- `react-router-dom@^4` to handle routing and components renderization based on routes paths
- `jest` to test our components
- `enzyme` to make expectations upon components content
- browser `localStorage` to store the recently done searches
- `semantic-ui` and `semantic-ui-react` to make things a little bit prettier

## Getting Started

### Prerequisites

- Node
- [Yarn](https://yarnpkg.com/en/docs/install)

### Installing

Just install all dependencies

```
yarn install
```

## Running local

This will open up your browser with the site running under a `webpack-dev-server` in development mode:

```
yarn start
```

## Running tests

Components and Sagas are tested using `Jest` and `Enzyme`, you can run them by:

```
yarn test
```

If a chance is made to one of the components it might be needed to update the snapshots, in order for the test to run based on the actual change.

To update snapshots (see [Jest snapshots](https://facebook.github.io/jest/docs/en/snapshot-testing.html))
```
yarn test-update
```