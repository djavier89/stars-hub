import React from 'react'
import {Route, Switch} from 'react-router-dom'
import ProfileHeaderContainer from '../containers/profileHeader'

class HeadContainer extends React.Component {
  render () {
    return <Switch>
      <Route exact path='/' render={() => <div />} />
      <Route path='/:username' component={ProfileHeaderContainer} />
    </Switch>
  }
}

export default HeadContainer
