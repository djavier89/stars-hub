import React from 'react'
import {Grid, Segment, Header} from 'semantic-ui-react'
import GithubProfileCard from '../components/githubProfileCard'
import GithubRepoList from '../components/githubRepoList'
import GithubUserList from '../components/githubUserList'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import * as actions from '../actions/github/actions'

import LoadingAware from '../components/hocs/loadingAware'

const GithubProfileCardWithLoading = LoadingAware(GithubProfileCard)
const GithubRepoListWithLoading = LoadingAware(GithubRepoList)
const GithubUserListWithLoading = LoadingAware(GithubUserList)

class ProfileContainer extends React.Component {
  componentDidMount () {
    this
      .props
      .githubActions
      .fetchExtendedProfile(this.props.match.params.username)
  }

  render () {
    return (
      <div style={{
        textAlign: 'center',
        margin: '5%'
      }}>
        <Grid container doubling stackable columns={2} centered>
          <Grid.Column>
            <GithubProfileCardWithLoading isLoading={this.props.loading} profile={this.props.profile} />
            { this.props.profile
              ? <div style={{marginTop: '20px'}}>
                <Header as='h2' icon='users' content='Following' />
                <Segment>
                  <GithubUserListWithLoading isLoading={this.props.loadingFollowing} users={this.props.profile.following_users} />
                </Segment>
                <Header as='h2' icon='users' content='Followers' />
                <Segment>
                  <GithubUserListWithLoading isLoading={this.props.loadingFollowers} users={this.props.profile.followers_users} />
                </Segment>
              </div>
              : <div /> }
          </Grid.Column>
          <Grid.Column>
            { this.props.profile
              ? <div>
                <Header as='h2' icon='star' content='Starred' />
                <GithubRepoListWithLoading isLoading={this.props.loadingStarred} repos={this.props.profile.starred} />
              </div>
              : <div /> }
          </Grid.Column>
        </Grid>
      </div>
    )
  }
}

function mapStateToProps ({github}) {
  return {
    profile: github.profile,
    loading: github.loading,
    loadingStarred: github.loadingStarred,
    loadingFollowing: github.loadingFollowing,
    loadingFollowers: github.loadingFollowers
  }
}

function mapDispatchToProps (dispatch) {
  return {
    githubActions: bindActionCreators(actions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileContainer)
