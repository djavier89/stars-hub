import React from 'react'
import ProfileHeaderMenu from '../components/profileHeaderMenu'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'

class ProfileHeaderContainer extends React.Component {
  render () {
    const {profile} = this.props
    return (
      profile
        ? <ProfileHeaderMenu profile={profile} />
        : <div />
    )
  }
}

ProfileHeaderContainer.propTypes = {
  profile: PropTypes.object
}

function mapStateToProps ({github}) {
  return {profile: github.profile}
}

export default connect(mapStateToProps)(ProfileHeaderContainer)
