import React from 'react'
import {Grid, Input, Header, Segment, List, Image} from 'semantic-ui-react'
import GithubProfileCard from '../components/githubProfileCard'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import * as actions from '../actions/github/actions'
import LoadingAware from '../components/hocs/loadingAware'
import moment from 'moment'
import {Link} from 'react-router-dom'

const GithubProfileCardWithLoading = LoadingAware(GithubProfileCard)

class ProfileSearchContainer extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      username: '',
      error: {
        touched: false,
        valid: false
      }
    }
    this.searchHandleChange = this.searchHandleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentDidMount () {
    this.props.githubActions.fetchSearchHistory()
  }

  isValid () {
    return (this.state.error.valid && this.state.error.touched) || !this.props.error
  }

  isProfile () {
    const {profile} = this.props
    return this.isValid() && profile && profile.login === this.state.username
  }

  searchHandleChange (event, {value}) {
    if (value.length === 0) {
      this.setState({username: value, error: {touched: true, valid: false}})
    } else {
      this.setState({username: value, error: { touched: true, valid: true }})
    }
  }

  handleSubmit (event) {
    if (this.isValid()) {
      this.props.githubActions.fetchProfile(this.state.username)
    }
    event.preventDefault()
  }

  renderHistoryList (history) {
    const historyItems = history.map((item) =>
      <List.Item key={item.username}>
        <Image avatar src={item.picture} />
        <List.Content>
          <List.Header>
            <Link to={`/${item.username}`}>
              {item.username}
            </Link>
          </List.Header>
          <List.Description>Last seen on <b>{moment(item.timestamp).fromNow()}</b></List.Description>
        </List.Content>
      </List.Item>
    )

    return (
      <List divided relaxed>
        {historyItems}
      </List>
    )
  }

  render () {
    const isValid = this.isValid()

    return (
      <div style={{marginTop: '20vh'}}>
        <form onSubmit={this.handleSubmit} style={{textAlign: 'center'}}>
          <Header as='h1'>Welcome to github stars</Header>
          <Header as='h3'>Find your hero here</Header>

          <Grid verticalAlign='middle' columns={4} centered>
            <Grid.Row centered columns={12} >
              <Grid.Column mobile={12} tablet={8} computer={6}>
                <Input error={!isValid} icon='search' size='huge' placeholder='Github username' fluid onChange={this.searchHandleChange} />
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </form>

        {this.props.error && !this.props.loading
        ? <Grid verticalAlign='middle' columns={4} centered>
          <Grid.Row centered columns={12}>
            <Grid.Column width={6}>
              <Segment inverted color='red'> {this.props.error}</Segment>
            </Grid.Column>
          </Grid.Row>
        </Grid>
        : <div />}

        { this.isProfile()
        ? <Grid verticalAlign='middle' columns={4} centered>
          <Grid.Row centered columns={12}>
            <Grid.Column mobile={10} tablet={8} computer={6}>
              <GithubProfileCardWithLoading isLoading={this.props.loading && !this.props.error} profile={this.props.profile} />
            </Grid.Column>
          </Grid.Row>
        </Grid>
        : null }

        { this.props.history.length
        ? <Grid verticalAlign='middle' columns={4} centered>
          <Grid.Row centered columns={12}>
            <Grid.Column mobile={12} tablet={8} computer={6}>
              <Segment>
                <Header as='h3' className='centered'>Recently searched</Header>
                {this.renderHistoryList(this.props.searchHistory)}
              </Segment>
            </Grid.Column>
          </Grid.Row>
        </Grid>
        : <div />
        }
      </div>
    )
  }
}

function mapStateToProps ({github}) {
  return {
    profile: github.profile,
    loading: github.loading,
    error: github.error,
    searchHistory: github.history
  }
}

function mapDispatchToProps (dispatch) {
  return {
    githubActions: bindActionCreators(actions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileSearchContainer)
