import React from 'react'
import ProfileSearchContainer from './profileSearch'
import ProfileContainer from './profile'
import {Route, Redirect, Switch} from 'react-router-dom'

const MainContainer = () => (
  <main>
    <Switch>
      <Route exact path='/' component={ProfileSearchContainer} />
      <Route path='/:username' component={ProfileContainer} />
      <Redirect to='/' />
    </Switch>
  </main>
)

export default MainContainer
