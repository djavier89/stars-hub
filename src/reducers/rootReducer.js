import {combineReducers} from 'redux'
import github from './github/githubReducer'

const rootReducer = combineReducers({ github })

export default rootReducer
