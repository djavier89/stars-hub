import * as types from '../../actions/github/actionTypes'

const initialState = {
  profile: undefined,
  error: undefined,
  loading: false,

  loadingStars: false,
  errorStars: false,

  loadingFollowing: false,
  errorFollowing: false,

  loadingFollowers: false,
  errorFollowers: false,

  history: []
}

export default function github (state = initialState, action) {
  switch (action.type) {
    case types.PROFILE_LOADING:
      return { ...state, loading: true }
    case types.PROFILE_SUCCEDED:
      return {
        ...state,
        loading: false,
        profile: action.payload,
        error: undefined
      }
    case types.PROFILE_FAILED:
      return {
        ...state,
        loading: false,
        error: action.payload
      }
    case types.STARRED_LOADING:
      return { ...state, loadingStars: true }
    case types.STARRED_SUCCEDED:
      return {
        ...state,
        loadingStars: false,
        profile: {
          ...state.profile,
          starred: action.payload
        },
        errorStars: undefined
      }
    case types.STARRED_FAILED:
      return {
        ...state,
        loadingStars: false,
        errorStars: action.payload
      }
    case types.FOLLOWING_LOADING:
      return { ...state, loadingFollowing: true }
    case types.FOLLOWING_SUCCEDED:
      return {
        ...state,
        loadingFollowing: false,
        profile: {
          ...state.profile,
          following_users: action.payload
        },
        errorFollowing: undefined
      }
    case types.FOLLOWING_FAILED:
      return {
        ...state,
        loadingFollowing: false,
        errorFollowing: action.payload
      }
    case types.FOLLOWERS_LOADING:
      return { ...state, loadingFollowers: true }
    case types.FOLLOWERS_SUCCEDED:
      return {
        ...state,
        loadingFollowers: false,
        profile: {
          ...state.profile,
          followers_users: action.payload
        },
        errorFollowers: undefined
      }
    case types.FOLLOWERS_FAILED:
      return {
        ...state,
        loadingFollowers: false,
        errorFollowers: action.payload
      }
    case types.ADD_TO_HISTORY:
      return { ...state, history: [...state.history, action.payload] }
    case types.FETCH_HISTORY_SUCCEDED:
      return { ...state, history: action.payload }
    default:
      return state
  }
}
