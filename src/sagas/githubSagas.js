import {put, call, select, fork} from 'redux-saga/effects'
import githubService from '../services/github/githubService'
import * as githubActions from '../actions/github/actions'

const HISTORY_STORAGE_KEY = 'GITHUB_SEARCH_HISTORY'
export const profileSelector = () => (state) => state.github.profile

export function * githubGetProfile ({username}) {
  yield put(githubActions.profileLoading())
  try {
    let profile = yield call(githubService.getProfile, username)
    if (profile) {
      yield put(githubActions.profileSuccess(profile))
      yield fork(githubAddSearchHistory, profile)
    }
  } catch (error) {
    yield put(githubActions.profileError(error.error))
  }
}

export function * githubGetStarredByUsername ({username}) {
  yield put(githubActions.starredLoading())
  try {
    let starred = yield call(githubService.getStarred, username)
    if (starred) {
      yield put(githubActions.starredSuccess(starred))
    }
  } catch (error) {
    yield put(githubActions.starredError(error))
  }
}

export function * githubGetFollowingByUsername ({username}) {
  yield put(githubActions.followingLoading())
  try {
    let users = yield call(githubService.getFollowing, username)
    if (users) {
      yield put(githubActions.followingSuccess(users))
    }
  } catch (error) {
    yield put(githubActions.followingError(error))
  }
}

export function * githubGetFollowersByUsername ({username}) {
  yield put(githubActions.followersLoading())
  try {
    let users = yield call(githubService.getFollowers, username)
    if (users) {
      yield put(githubActions.followersSuccess(users))
    }
  } catch (error) {
    yield put(githubActions.followingError(error))
  }
}

export function * githubExtendedProfile ({username}) {
  const profile = yield select(profileSelector)
  if (typeof (profile) === 'undefined' || (profile && profile.login !== username)) {
    yield fork(githubGetProfile, {username})
  }

  yield fork(githubGetStarredByUsername, {username})
  yield fork(githubGetFollowingByUsername, {username})
  yield fork(githubGetFollowersByUsername, {username})
}

export function * githubAddSearchHistory (profile) {
  let searchHistory = window.localStorage.getItem(HISTORY_STORAGE_KEY)
  searchHistory = searchHistory !== null ? JSON.parse(searchHistory) : []

  searchHistory = searchHistory.map((history) => {
    if (history.username === profile.login) {
      history.timestamp = Date.now()
      return history
    } else return history
  })

  if (searchHistory.filter(item => item.username === profile.login).length === 0) {
    searchHistory.push({username: profile.login, picture: profile.avatar_url, timestamp: Date.now()})
  }
  window.localStorage.setItem(HISTORY_STORAGE_KEY, JSON.stringify(searchHistory))
}

export function * githubSearchHistory () {
  let searchHistory = window.localStorage.getItem(HISTORY_STORAGE_KEY)
  searchHistory = searchHistory !== null ? JSON.parse(searchHistory) : []

  yield put(githubActions.fetchSearchHistorySuccess(searchHistory))
}
