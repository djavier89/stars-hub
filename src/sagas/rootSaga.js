import {takeLatest, all} from 'redux-saga/effects'
import { PROFILE_FETCH,
  STARRED_FETCH,
  FOLLOWING_FETCH,
  FOLLOWERS_FETCH,
  EXTENDED_PROFILE_FETCH,
  FETCH_HISTORY
} from '../actions/github/actionTypes'

import * as githubSagas from './githubSagas'

export default function * rootSaga () {
  yield all([
    takeLatest(PROFILE_FETCH, githubSagas.githubGetProfile),
    takeLatest(STARRED_FETCH, githubSagas.githubGetStarredByUsername),
    takeLatest(FOLLOWING_FETCH, githubSagas.githubGetFollowingByUsername),
    takeLatest(FOLLOWERS_FETCH, githubSagas.githubGetFollowersByUsername),
    takeLatest(EXTENDED_PROFILE_FETCH, githubSagas.githubExtendedProfile),
    takeLatest(FETCH_HISTORY, githubSagas.githubSearchHistory)
  ])
}
