import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import configureStore from './store/configureStore'
import {BrowserRouter as Router} from 'react-router-dom'

import HeadContainer from './containers/head'
import MainContainer from './containers/main'

const store = configureStore()

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <div>
        <HeadContainer />
        <MainContainer />
      </div>
    </Router>
  </Provider>,
  document.getElementById('root')
)
