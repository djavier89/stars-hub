class GithubService {
  getProfile (username) {
    return require('./fixtures/profile.json')
  }

  getStarred (username) {
    return require('./fixtures/starred.json')
  }

  getFollowing (username) {
    return require('./fixtures/following.json')
  }

  getFollowers (username) {
    return require('./fixtures/followers.json')
  }
}

export default new GithubService()
