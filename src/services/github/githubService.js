import axios from 'axios'

export const BASE_URL = 'https://api.github.com'
export const BASE_SETTINGS = {
  headers: {
    'Accept': 'application/json'
  }
}

class GithubService {
  getProfile (username) {
    return axios
      .get(`${BASE_URL}/users/${username}`, BASE_SETTINGS)
      .then(response => response.data)
      .catch(error => {
        if (error.response.status === 404) {
          return Promise.reject({error: 'Username not found in github', status: error.response.status})
        } else {
          return Promise.reject({error: 'There was an error with the request.', status: error.response.status})
        }
      })
  }

  getStarred (username) {
    return axios
    .get(`${BASE_URL}/users/${username}/starred`, BASE_SETTINGS)
      .then(response => response.data)
      .catch(error => {
        throw error.response
      })
  }

  getFollowing (username) {
    return axios
      .get(`${BASE_URL}/users/${username}/following`, BASE_SETTINGS)
      .then(response => response.data)
      .catch(error => {
        throw error.response
      })
  }

  getFollowers (username) {
    return axios
      .get(`${BASE_URL}/users/${username}/followers`, BASE_SETTINGS)
      .then(response => response.data)
      .catch(error => {
        throw error.response
      })
  }
}

export default new GithubService()
