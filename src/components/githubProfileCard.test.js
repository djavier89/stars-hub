import React from 'react'
import GitHubProfileCard from './githubProfileCard'
import renderer from 'react-test-renderer'

it('renders correctly', () => {
  var profile = require('../services/github/fixtures/profile.json')
  const component = renderer
    .create(<GitHubProfileCard profile={profile} />)
    .toJSON()
  expect(component).toMatchSnapshot()
})
