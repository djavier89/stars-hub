import React from 'react'
import PropTypes from 'prop-types'
import {Menu, Icon} from 'semantic-ui-react'

class ProfileHeaderMenu extends React.Component {
  render () {
    return (
      <Menu inverted attached='top'>
        <Menu.Menu position='right'>
          <div className='ui right aligned category search item'>
            <div className='ui transparent icon input'>
              {this.props.profile
              ? <a target='blank' href={`${this.props.profile.html_url}`}>
                <Icon as='i' name='github' size='big' />
                View on Github
              </a>
              : <div />
              }
            </div>
          </div>
        </Menu.Menu>
      </Menu>
    )
  }
}

ProfileHeaderMenu.propTypes = {
  profile: PropTypes.object
}

export default ProfileHeaderMenu
