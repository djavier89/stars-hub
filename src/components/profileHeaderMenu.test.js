import React from 'react'
import ProfileHeaderMenu from './profileHeaderMenu'
import renderer from 'react-test-renderer'
import {Menu} from 'semantic-ui-react'
import {shallow} from 'enzyme'

describe ('<ProfileHeaderMenu />', () => {
  const profile = require('../services/github/fixtures/profile.json')

  it('renders correctly', () => {
    const component = renderer
      .create(<ProfileHeaderMenu profile={profile} />)
      .toJSON()
    expect(component).toMatchSnapshot()
  })

  it('renders a link to Github', () => {
    const wrapper = shallow(<ProfileHeaderMenu profile={profile} />)
    expect(wrapper.find(Menu)).toHaveLength(1)
    expect(wrapper.find('a')).toHaveLength(1)
  })
})
