import PropTypes from 'prop-types'
import {Image, Card, Icon} from 'semantic-ui-react'
import React from 'react'
import {Link} from 'react-router-dom'

export default class GithubProfileCard extends React.Component {
  renderProfileCard (profile) {
    if (profile) {
      return (<a href={`/${this.props.profile.login}`}>
        <Card centered>
          <Image src={profile.avatar_url} />
          <Card.Content>
            <Card.Header>
              @{profile.login}
            </Card.Header>
            <Card.Meta>
              <span className='date'>
                {profile.public_repos} Repos
              </span>
            </Card.Meta>
            <Card.Description>
              {profile.bio}
            </Card.Description>
          </Card.Content>
          <Card.Content extra>
            <div>
              <Icon name='user' />
              {profile.followers} Followers
            </div>
            <div>
              <Icon name='user' />
              {profile.following} Following
            </div>
            <div>
              <Icon name='file code outline' />
              {profile.public_gists} Gists
            </div>
          </Card.Content>
        </Card>
      </a>
      )
    }
  }

  render () {
    let profile = this.props.profile
    return (
      <div className=''>
        {this.renderProfileCard(profile)}
      </div>
    )
  }
}

GithubProfileCard.propTypes = {
  profile: PropTypes.object
}
