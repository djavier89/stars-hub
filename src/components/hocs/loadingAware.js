import React from 'react'
import {Grid, Loader} from 'semantic-ui-react'

function LoadingAware (Component) {
  return function LoadingAwareComponent ({isLoading, ...props}) {
    if (!isLoading) {
      return (<Component {...props} />)
    }
    return (
      <Grid verticalAlign='middle' columns={4} centered>
        <Grid.Row centered columns={12}>
          <Grid.Column width={6}>
            <Loader indeterminate active={isLoading}> Loading content, please wait.</Loader>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )
  }
}

export default LoadingAware
