import React from 'react'
import LoadingAware from './loadingAware'
import renderer from 'react-test-renderer'

const WithLoading = LoadingAware(() => <div />)

it('renders correctly when is loading', () => {
  const component = renderer
    .create(<WithLoading isLoading />)
    .toJSON()
  expect(component).toMatchSnapshot()
})

it('renders correctly when is not loading', () => {
  const component = renderer
    .create(<WithLoading isLoading={false} />)
    .toJSON()
  expect(component).toMatchSnapshot()
})