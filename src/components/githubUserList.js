import PropTypes from 'prop-types'
import {Item} from 'semantic-ui-react'
import React from 'react'

export default class GithubUserList extends React.Component {
  renderList (users) {
    const userItems = users.map((user) =>
      <Item key={user.id}>
        <Item.Image size='tiny' src={user.avatar_url} />
        <Item.Content verticalAlign='middle'>
          <Item.Header><a href={`/${user.login}`}>{user.login}</a></Item.Header>
        </Item.Content>
      </Item>
    )

    return (
      <Item.Group divided>
        {userItems}
      </Item.Group>
    )
  }

  render () {
    let users = this.props.users
    return (
      <div className=''>
        { users ? this.renderList(users) : null}
      </div>
    )
  }
}

GithubUserList.propTypes = {
  users: PropTypes.array
}
