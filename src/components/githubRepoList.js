import PropTypes from 'prop-types'
import {Icon, Item, Label} from 'semantic-ui-react'
import React from 'react'

export default class GithubStarredList extends React.Component {
  renderList (repos) {
    const repoItems = repos.map((repo) =>
      <Item key={repo.id}>
        <Icon name='github' size='large' />
        <Item.Content>
          <Item.Header as='a'>{repo.name}</Item.Header>
          <Item.Meta>
            <span className='cinema'>{repo.language}</span>
          </Item.Meta>
          <Item.Description>{repo.description}</Item.Description>
          <Item.Extra>
            <Label icon='fork' content={repo.forks} />
            <Label icon='eye' content={repo.watchers} />
            {repo.license ? <Label> {repo.license.name} </Label> : null }
          </Item.Extra>
        </Item.Content>
      </Item>
    )

    return (
      <Item.Group divided>
        {repoItems}
      </Item.Group>
    )
  }

  render () {
    let repos = this.props.repos
    return (
      <div className=''>
        { repos ? this.renderList(repos) : null}
      </div>
    )
  }
}

GithubStarredList.propTypes = {
  repos: PropTypes.array
}
