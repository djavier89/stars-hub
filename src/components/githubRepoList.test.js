import React from 'react'
import GithubRepoList from './githubRepoList'
import renderer from 'react-test-renderer'

it('renders correctly', () => {
  var repos = require('../services/github/fixtures/starred.json')
  const component = renderer
    .create(<GithubRepoList repos={repos} />)
    .toJSON()
  expect(component).toMatchSnapshot()
})
