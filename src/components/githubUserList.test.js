import React from 'react'
import GithubUserList from './githubUserList'
import renderer from 'react-test-renderer'

it('renders correctly followers users', () => {
  var users = require('../services/github/fixtures/followers.json')
  const component = renderer
    .create(<GithubUserList users={users} />)
    .toJSON()
  expect(component).toMatchSnapshot()
})

it('renders correctly follwing users', () => {
  var users = require('../services/github/fixtures/following.json')
  const component = renderer
    .create(<GithubUserList users={users}/>)
    .toJSON()
  expect(component).toMatchSnapshot()
})
