import * as types from './actionTypes'

export function fetchProfile (username) {
  return { type: types.PROFILE_FETCH, username }
}
export function profileLoading () {
  return {type: types.PROFILE_LOADING}
}

export function profileSuccess (payload) {
  return {type: types.PROFILE_SUCCEDED, payload}
}

export function profileError (payload) {
  return {type: types.PROFILE_FAILED, payload}
}

export function fetchStarred (username) {
  return {type: types.STARRED_FETCH, username}
}

export function starredLoading () {
  return {type: types.STARRED_LOADING}
}

export function starredSuccess (payload) {
  return {type: types.STARRED_SUCCEDED, payload}
}

export function starredFailed (payload) {
  return {type: types.STARRED_FAILED, payload}
}

export function fetchFollowing (username) {
  return {type: types.FOLLOWING_FETCH, username}
}

export function followingLoading () {
  return {type: types.FOLLOWING_LOADING}
}

export function followingSuccess (payload) {
  return {type: types.FOLLOWING_SUCCEDED, payload}
}

export function followingFailed (payload) {
  return {type: types.FOLLOWING_FAILED, payload}
}

export function fetchFollowers (username) {
  return {type: types.FOLLOWERS_FETCH, username}
}

export function followersLoading () {
  return {type: types.FOLLOWERS_LOADING}
}

export function followersSuccess (payload) {
  return {type: types.FOLLOWERS_SUCCEDED, payload}
}

export function followersFailed (payload) {
  return {type: types.FOLLOWERS_FAILED, payload}
}

export function fetchExtendedProfile (username) {
  return {type: types.EXTENDED_PROFILE_FETCH, username}
}

export function fetchSearchHistory () {
  return {type: types.FETCH_HISTORY}
}

export function fetchSearchHistorySuccess (payload) {
  return {type: types.FETCH_HISTORY_SUCCEDED, payload}
}
